//
//  Utils.swift
//  iTunesPlayer
//
//  Created by Sergiu Juravle on 04/05/2018.
//  Copyright © 2018 Assist. All rights reserved.
//

import Foundation

class Utils {
    
    static func formatTime(timeMillis: Int64) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeMillis / 1000))
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        return formatter.string(from: date)
    }
}
