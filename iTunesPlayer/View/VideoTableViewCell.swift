//
//  VideoTableViewCell.swift
//  iTunesPlayer
//
//  Created by Sergiu Juravle on 02/05/2018.
//  Copyright © 2018 Assist. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var videoName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var videoTime: UILabel!
    
    // MARK: Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
