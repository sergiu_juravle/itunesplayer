//
//  ViewController.swift
//  iTunesVideoPlayer
//
//  Created by Sergiu Juravle on 26/04/2018.
//  Copyright © 2018 Assist. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher
import AVKit
import AVFoundation

class UISearchController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Properties
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emptyView: UILabel!
    
    let disposeBag = DisposeBag()
    var videoModel: VideoModel!
    
    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupRx()
    }
    
    // MARK: TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoModel.getDataCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell", for: indexPath) as! VideoTableViewCell
        let item = videoModel.getItem(indexPath.row)
        
        cell.videoImage.kf.setImage(with: item.artworkUrl, options: [.transition(.fade(0.3))])
        cell.videoName.text = item.trackName
        cell.artistName.text = item.artistName
        cell.videoTime.text = Utils.formatTime(timeMillis: item.trackTimeMilis)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == videoModel.getDataCount() - 1 && !videoModel.searchDone {
            videoModel
                .findVideos()
                .subscribe(onNext: { self.addAll(videos: $0) })
                .disposed(by: disposeBag)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoUrl = videoModel.getItem(indexPath.row).previewUrl!
        playVideo(videoUrl)
    }
    
    // MARK: Private methods
    
    private func setupViews() {
        navBar.topItem?.title = "iTunesPlayer"
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupRx() {
        
        videoModel = VideoModel()
        
        searchBar
            .rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { text in
                
                self.tableView.isHidden = true
                self.emptyView.isHidden = true
                self.resetData()
                
                if !text.isEmpty {
                    self.activityIndicator.startAnimating()
                }
                
                self.findVideos(text)
            })
            .disposed(by: disposeBag)
        
        tableView
            .rx.itemSelected
            .subscribe(
                onNext: { _ in
                    if self.searchBar.isFirstResponder {
                        self.view.endEditing(true)
                    }
            })
            .disposed(by: disposeBag)
    }
    
    private func findVideos(_ text: String) {
        self.videoModel
            .findVideos(videoName: text)
            .subscribe(onNext: { videos in
                
                self.activityIndicator.stopAnimating()
                
                if !videos.isEmpty {
                    self.tableView.isHidden = false
                } else {
                    self.emptyView.isHidden = false
                }
                
                self.addAll(videos: videos)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func addAll(videos: [Video]) {
        videoModel.addAll(videos)
        tableView.reloadData()
    }
    
    private func resetData() {
        self.videoModel.resetData()
        self.tableView.reloadData()
    }
    
    private func playVideo(_ url: URL) {
        let player = AVPlayer(url: url)
        
        let controller = AVPlayerViewController()
        controller.player = player
        
        present(controller, animated: true) {
            player.play()
        }
    }
}

