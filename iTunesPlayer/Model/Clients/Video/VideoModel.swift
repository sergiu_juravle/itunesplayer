//
//  iTunesClient.swift
//  iTunesVideoPlayer
//
//  Created by Sergiu Juravle on 26/04/2018.
//  Copyright © 2018 Assist. All rights reserved.
//
import RxSwift
import RxAlamofire
import Alamofire
import SwiftyJSON

class VideoModel {
    
    // MARK: Properties
    
    var videoData = [Video]()
    var lastVideoName = ""
    var offset = 0
    var searchDone = false
    
    // MARK: Api Methods
    
    func findVideos(videoName: String? = nil) -> Observable<[Video]>{
        let url = URL(string: Constants.BaseUrl + Methods.Search)
        
        if videoName != nil {
            lastVideoName = videoName!
        }
        
        return Observable.just(lastVideoName)
            .observeOn(MainScheduler.instance)
            .flatMap { name -> Observable<(HTTPURLResponse, Any)> in
                
                if name.isEmpty {
                    return Observable.empty()
                }
                
                let parameters: [String: Any] = [
                    ParameterKeys.Term : name,
                    ParameterKeys.Entity: ParameterValues.Entity,
                    ParameterKeys.Offset: self.offset,
                    ParameterKeys.Limit: ParameterValues.Limit
                ]
                print("Current offset: \(self.offset)")
                return RxAlamofire.requestJSON(.get, url!, parameters: parameters)
            }
            .map { (response, result) -> [Video] in
                
                guard response.statusCode >= 200 && response.statusCode <= 299 else {
                    self.searchDone = true
                    throw NetworkError.statusCodeError("Request returned status code: \(response.statusCode).")
                }
                
                let json = JSON(result)
                let jsonArray = json[ResponseKeys.Results].arrayValue
                
                var videoArray: [Video] = []
                
                for jsonVideo in jsonArray {
                    videoArray.append(Video(json: jsonVideo))
                }
                
                if videoArray.count < ParameterValues.Limit {
                    self.searchDone = true
                }
                
                self.offset += ParameterValues.Limit
                
                return videoArray
            }
            .catchError { error in
                print(error)
                return Observable.just([])
        }
    }
    
    // MARK: Data Manager Functions
    
    /*
     Gets item at specified postion
    */
    func getItem(_ position: Int) -> Video{
        return videoData[position]
    }
    
    /*
     Gets the number of videos
    */
    func getDataCount() -> Int{
        return videoData.count
    }
    
    /*
     Appends a list of Video to the main list
    */
    func addAll(_ videos: [Video]) {
        videoData += videos
    }
    
    /*
     Clears all data and reinitializes the properties
    */
    func resetData() {
        videoData = []
        offset = 0
        searchDone = false
    }
    
    /*
     Used for network handling
    */
    enum NetworkError: Error {
        case statusCodeError(String)
    }
}
