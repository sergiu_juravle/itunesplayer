//
//  Movie.swift
//  iTunesVideoPlayer
//
//  Created by Sergiu Juravle on 28/04/2018.
//  Copyright © 2018 Assist. All rights reserved.
//
import SwiftyJSON
import Alamofire

struct Video {
    
    // MARK: Properties
    
    let trackName: String
    let artistName: String
    var trackTimeMilis: Int64
    var previewUrl: URL? = nil
    var artworkUrl: URL? = nil
  
    // MARK: Initializers
    
    init(json: JSON) {
        trackName = json[ResponseKeys.TrackName].stringValue
        artistName = json[ResponseKeys.ArtistName].stringValue
        trackTimeMilis = json[ResponseKeys.TrackTimeMillis].int64Value
        
        let previewUrlString = json[ResponseKeys.PreviewUrl].stringValue
        do {
            previewUrl = try previewUrlString.asURL()
        } catch {
            print("Could not convert to url \(previewUrlString)")
        }
        
        let artworkString = json[ResponseKeys.ArtworkUrl].stringValue
        do {
            artworkUrl = try artworkString.asURL()
        } catch {
            print("Could not convert to url \(artworkString)")
        }
    }
}
