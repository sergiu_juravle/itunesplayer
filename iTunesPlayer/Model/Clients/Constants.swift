//
//  Constants.swift
//  iTunesVideoPlayer
//
//  Created by Sergiu Juravle on 27/04/2018.
//  Copyright © 2018 Assist. All rights reserved.
//

import Foundation


// MARK: Constants

struct Constants {
    static let BaseUrl = "http://itunes.apple.com"
}

// MARK: Methods

struct Methods {
    static let Search = "/search"
}

// MARK: Parameter Keys

struct ParameterKeys {
    
    // MARK: General
    
    
    
    // MARK: Search
    
    static let Term = "term"
    static let Entity = "entity"
    static let Offset = "offset"
    static let Limit = "limit"
}

// MARK: Parameter Values

struct ParameterValues {
    
    // MARK: Search
    
    static let Entity = "musicVideo"
    static let Limit = 20
}

// MARK: Response Keys

struct ResponseKeys {
    static let Results = "results"
    static let TrackName = "trackName"
    static let ArtistName = "artistName"
    static let TrackTimeMillis = "trackTimeMillis"
    static let PreviewUrl = "previewUrl"
    static let ArtworkUrl = "artworkUrl60"
}

