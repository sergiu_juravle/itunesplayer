//
//  Convenience.swift
//  iTunesVideoPlayer
//
//  Created by Sergiu Juravle on 27/04/2018.
//  Copyright © 2018 Assist. All rights reserved.
//
import Alamofire
import SwiftyJSON

extension RestClient {
    
    // MARK: Search Api Method
    
    func getVideosForSearchString(_ searchString: String, completionHandler: @escaping (_ result: JSON?, _ errorString: String? ) -> Void) {
        let url = URL(string: Constants.ApiHost + Methods.Search)
        let parameters = [
            ParameterKeys.Term : searchString
        ]
        
        createRequest(
            httpMethod: .get,
            url: url!,
            parameters: parameters as [String : Any],
            completionHandler: completionHandler
        )
    }
}
